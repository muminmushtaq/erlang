            % using Recursion

-module(n4).
-export([create/1]).

create(N) when N == 0-> 0;
create(N) when N > 0 -> 
    create(1,N).

create(Start,End) when Start >= End-> End;
create(Start,End) when Start < End ->
    io:format("~p~n",[Start]),
    create(Start+1,End).
