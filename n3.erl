            % Using Recursion

-module(n3).
-export([create/2]).

create(Start,End) when Start >= End -> End;

create(Start,End) when Start < End -> 
    io:format("~p~n",[Start]),
    create(Start+1,End).