            % Using Recursion

-module(nat2).
-export([limit/1]).

limit(B) when B = 0 -> 1;
    %io:format("~p~n",[0])

limit(B) when B < 11 ->
    io:format("~p~n",[B]),
   % io:format("~p~n",[B]),
    limit(B + 1).
   